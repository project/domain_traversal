<?php

namespace Drupal\domain_traversal\Controller;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\Condition;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\Url;
use Drupal\domain\DomainInterface;
use Drupal\domain\DomainNegotiatorInterface;
use Drupal\domain\Entity\Domain;
use Drupal\domain_traversal\DomainTraversalInterface;
use Drupal\user\UserInterface;
use Drupal\user\UserStorageInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DomainTraversal extends ControllerBase {

  /**
   * Active database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The domains traversal service.
   *
   * @var \Drupal\domain_traversal\DomainTraversalInterface
   */
  protected $domainTraversal;

  /**
   * The domains negotiator.
   *
   * @var \Drupal\domain\DomainNegotiatorInterface
   */
  protected $domainNegotiator;

  /**
   * @var \Psr\Log\LoggerInterface
   */
  private $logger;

  /**
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  private $time;

  /**
   * @var \Drupal\user\UserStorageInterface
   */
  private $userStorage;

  // @todo convert to config
  protected $secretTimeout = 30;

  /**
   * @param \Drupal\Core\Database\Connection $database
   * @param \Drupal\domain_traversal\DomainTraversalInterface $domainTraversal
   * @param \Drupal\domain\DomainNegotiatorInterface $domainNegotiator
   * @param \Psr\Log\LoggerInterface $logger
   * @param \Drupal\Component\Datetime\TimeInterface $time
   * @param \Drupal\user\UserStorageInterface $userStorage
   */
  public function __construct(Connection $database, DomainTraversalInterface $domainTraversal, DomainNegotiatorInterface $domainNegotiator, LoggerInterface $logger, TimeInterface $time, UserStorageInterface $userStorage) {
    $this->database = $database;
    $this->domainTraversal = $domainTraversal;
    $this->domainNegotiator = $domainNegotiator;
    $this->logger = $logger;
    $this->time = $time;
    $this->userStorage = $userStorage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('domain_traversal'),
      $container->get('domain.negotiator'),
      $container->get('logger.channel.domain_traversal'),
      $container->get('datetime.time'),
      $container->get('entity_type.manager')->getStorage('user')
    );
  }

  /**
   * Redirects the user to the domain with a secret-key to login.
   *
   * @param \Drupal\domain\Entity\Domain $domain
   *
   * @return \Drupal\Core\Routing\TrustedRedirectResponse
   *
   * @throws \Exception
   */
  public function traverse(Domain $domain): TrustedRedirectResponse {
    /** @var \Drupal\user\UserInterface $account */
    $account = $this->userStorage->load(\Drupal::service('current_user')->id());
    $timestamp = $this->time->getRequestTime();
    $secret = $this->secretKey($account, $domain->id(), $timestamp);

    $params = [
      'uid' => $account->id(),
      'domain' => $domain->id(),
      'timestamp' => $timestamp,
      'secret' => $secret,
    ];

    $this->database->insert('domain_traversal')->fields($params)->execute();

    $options = [
      'base_url' => trim($domain->getPath(), '/'),
      'absolute' => TRUE,
    ];
    $url = Url::fromRoute('domain_traversal.login', $params, $options);

    return new TrustedRedirectResponse($url->toString(TRUE)->getGeneratedUrl());
  }

  /**
   * Checks if the user has more than 1 domain to traverse to.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *
   * @return \Drupal\Core\Access\AccessResult
   */
  public function domainTraversalAccess(AccountInterface $account): AccessResult {
    /** @var \Drupal\user\UserInterface $loadedAccount */
    $loadedAccount = $this->userStorage->load($account->id());
    if ($this->domainTraversal->accountMayTraverseAllDomains($loadedAccount)) {
      return AccessResult::allowed();
    }

    $domainIds = $this->domainTraversal->getAccountTraversableDomainIds($loadedAccount);
    return AccessResult::allowedIf(count($domainIds) > 1);
  }

  /**
   * Checks if the user may access the traverse callback.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   * @param \Drupal\domain\Entity\Domain $domain
   *
   * @return \Drupal\Core\Access\AccessResult
   */
  public function traverseAccess(AccountInterface $account, Domain $domain): AccessResult {
    // You can't "traverse" to the current domain.
    if ($domain->id() === $this->domainNegotiator->getActiveId()) {
      return AccessResult::forbidden();
    }

    if ($account->isAnonymous()) {
      return AccessResult::forbidden();
    }

    if ($account->hasPermission('traverse all domains')) {
      return AccessResult::allowed();
    }

    return AccessResult::allowedIfHasPermission($account, 'traverse domains')
      ->andIf(AccessResult::allowedIf($this->domainTraversal->accountMayTraverseDomain($this->userStorage->load($account->id()), $domain)));
  }

  /**
   * Tries to login the user with the provided secret.
   *
   * @param \Drupal\domain\DomainInterface $domain
   * @param int $uid
   * @param int $timestamp
   * @param string $secret
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function login(DomainInterface $domain, int $uid, int $timestamp, string $secret): RedirectResponse {
    $currentUser = $this->userStorage->load($this->currentUser()->id());
    $requestTime = $this->time->getRequestTime();

    if ($currentUser && !$currentUser->isAnonymous()) {
      // Someone is already logged in.
      if ((int) $currentUser->id() === $uid) {
        // It me!
        $this->cleanupSecret($domain, $timestamp, $secret, $requestTime);
        $this->messenger()->addMessage($this->t('You are now logged in.'));
      }
      else {
        // Someone else is already logged in.
        $this->messenger()
          ->addWarning($this->t('Another user is already logged into the site on this computer. Please <a href=":logout">log out</a> and try using the link again.', [
            ':logout' => Url::fromRoute('user.logout'),
          ]));
      }

      return $this->redirect('<front>');
    }

    /** @var \Drupal\user\UserInterface $account */
    $account = $this->userStorage->load($uid);
    user_login_finalize($account);
    $this->logger
      ->notice('User %name used one-time domain-traversal link at time %timestamp.', [
        '%name' => $account->getDisplayName(),
        '%timestamp' => $timestamp,
      ]);

    $this->messenger()->addMessage($this->t('You are now logged in.'));

    return $this->redirect('<front>');
  }

  /**
   * Grants access to login the user with the provided secret.
   *
   * @param \Drupal\domain\Entity\Domain $domain
   * @param int $uid
   * @param int $timestamp
   * @param string $secret
   *
   * @return \Drupal\Core\Access\AccessResult
   */
  public function loginAccess(Domain $domain, int $uid, int $timestamp, string $secret): AccessResult {
    $requestTime = $this->time->getRequestTime();

    $invalidLinkMessage = $this->t('You have tried to use an invalid one-time-domain-traversal link.');

    if ($timestamp < $requestTime - $this->secretTimeout) {
      return $this->accessDenied($this->t('You have tried to use a one-time-domain-traversal link that has expired.'));
    }

    $accountId = (int) $this->database->select('domain_traversal', 'dt')
      ->fields('dt', ['uid'])
      ->range(0, 1)
      ->condition('domain', $domain->id())
      ->condition('timestamp', $timestamp)
      ->condition('secret', $secret)
      ->execute()
      ->fetchField();

    $this->cleanupSecret($domain, $timestamp, $secret, $requestTime);

    if (0 < $accountId && $accountId !== $uid) {
      return $this->accessDenied($invalidLinkMessage);
    }

    /** @var \Drupal\user\UserInterface $account */
    $account = $this->userStorage->load($uid);
    if (!$account->isActive()) {
      return $this->accessDenied($invalidLinkMessage);
    }

    if (!$this->domainTraversal->accountMayTraverseDomain($account, $domain)) {
      return $this->accessDenied($this->t('You do not have access to the requested domain.'));
    }

    $secretKey = $this->secretKey($account, $domain->id(), $timestamp);
    if (!hash_equals($secret, $secretKey)) {
      return $this->accessDenied($invalidLinkMessage);
    }

    return AccessResult::allowed();
  }

  /**
   * Shows an error message and throws access denied exception.
   *
   * @param string $message
   *
   * @return \Drupal\Core\Access\AccessResult
   */
  protected function accessDenied(string $message): AccessResult {
    $this->messenger()->addError($message);

    return AccessResult::forbidden();
  }

  /**
   * Create a secret string.
   *
   * @param \Drupal\user\UserInterface $account
   * @param string $domainId
   * @param int $timestamp
   *
   * @return string
   */
  protected function secretKey(UserInterface $account, string $domainId, int $timestamp): string {
    return Crypt::hmacBase64($timestamp . $account->id(), Settings::getHashSalt() . $domainId . $account->getPassword());
  }

  /**
   * Cleanup the used secret and other expired secrets.
   *
   * @param \Drupal\domain\DomainInterface $domain
   * @param int $timestamp
   * @param string $secret
   * @param int $requestTime
   */
  protected function cleanupSecret(DomainInterface $domain, int $timestamp, string $secret, int $requestTime): void {
    $and = new Condition('AND');
    $and->condition('domain', $domain->id())
      ->condition('timestamp', $timestamp)
      ->condition('secret', $secret);

    $or = new Condition('OR');
    $or->condition($and)
      ->condition('timestamp', $requestTime - $this->secretTimeout, '<');

    $this->database->delete('domain_traversal')
      ->condition($or)
      ->execute();
  }

}
