<?php

namespace Drupal\domain_traversal;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\domain\Entity\Domain;
use Drupal\user\UserInterface;

class DomainTraversal implements DomainTraversalInterface {

  /**
   * @var \Drupal\Core\Entity\EntityStorageInterface $domainStorage
   */
  protected $domainStorage;

  /**
   * Whether the domain_access module is enabled.
   *
   * @var bool
   */
  protected $domainAccessEnabled;

  /**
   * The domains access manager service.
   *
   * @var \Drupal\domain_access\DomainAccessManagerInterface
   */
  protected $domainAccess;

  /**
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, ModuleHandlerInterface $moduleHandler) {
    $this->domainStorage = $entityTypeManager->getStorage('domain');
    $this->domainAccessEnabled = $moduleHandler->moduleExists('domain_access');

    if ($this->domainAccessEnabled) {
      $this->domainAccess = \Drupal::service('domain_access.manager');
    }
  }

  /**
   * @inheritdoc
   */
  public function getAccountTraversableDomainIds(UserInterface $account): array {
    $accountDomainIds = NULL;
    if ($this->domainAccessEnabled && !$this->accountMayTraverseAllDomains($account)) {
      $accountDomainIds = array_keys($this->domainAccess->getAccessValues($account));
    }

    $domainIds = [];

    /** @var \Drupal\domain\DomainInterface $domain */
    foreach ($this->domainStorage->loadMultiple($accountDomainIds) as $domain) {
      if (!$domain->status()) {
        continue;
      }
      $domainIds[$domain->id()] = $domain->id();
    }

    return $domainIds;
  }

  /**
   * @inheritdoc
   */
  public function accountMayTraverseDomain(UserInterface $account, Domain $domain): bool {
    if ($account->isAnonymous()) {
      return FALSE;
    }

    if ($this->accountMayTraverseAllDomains($account)) {
      return TRUE;
    }

    $domainIds = $this->getAccountTraversableDomainIds($account);

    return isset($domainIds[$domain->id()]);
  }

  /**
   * @inheritdoc
   */
  public function accountMayTraverseAllDomains(UserInterface $account): bool {
    if ($account->isAnonymous()) {
      return FALSE;
    }

    if ($account->hasPermission('traverse all domains')) {
      return TRUE;
    }

    if ($this->domainAccessEnabled && !empty($this->domainAccess->getAllValue($account))) {
      return TRUE;
    }

    return FALSE;
  }

}
